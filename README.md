RPA Extractor
=============


Description
-----------

Extract data archives often found in [Renpy](http://www.renpy.org/)
based games. They usually contain images and audio used in the game.
Only supports RPA v3.


Motivation
----------

There were some backgrounds from a game that I wanted to use as a
desktop wallpaper, so "reverse engineered" the [archiver](https://github.com/renpy/renpy/blob/master/launcher/game/archiver.rpy)
and extracted the media (in quotes because reverse engineering open
source is a joke). Authors usually archive the data for a reason, so use
responsibly.


Usage
-----

The data archives are located in the `game` folder in the game directory
and have the *rpa* file extension.

    python extract.py awesome-game-1.0-all/game/archive.rpa [path/to/destination]


The files will be extracted into the current directory, unless an
alternate destination is specified (which will be created if it does not
exist).
