#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# License: WTFPL 2.0
# See COPYING for details.


from cPickle import loads
import sys, os #, shutil


class RPA:
    """Renpy data archive extractor.

    See https://github.com/renpy/renpy/blob/master/launcher/game/archiver.rpy
    for more information.
    """

    HEADER = 'RPA-3.0'
    OFFSET_LENGTH = 16
    KEY = 0x42424242


    def __init__(self, path):
        """Constructor."""

        self.fh = open(path, 'rb')
        self.__verify()
        self.__loadIndex()


    def list(self):
        """Return the list of all files in the archive."""

        return self.index.keys()


    def get(self, name):
        """Get the offset and length of the requested file."""

        data = self.index[name]
        offset, length, blank = data[0]
        return (offset ^ self.__class__.KEY, length ^ self.__class__.KEY)


    def extract(self, name, path='.'):
        """Extract the specified file into the given location."""

        path = os.path.abspath(path + '/' + name)
        dir = os.path.dirname(path)

        if not os.path.exists(dir):
            os.makedirs(dir)

        offset, length = self.get(name)

        with open(path, 'wb') as target_fh:
            self.fh.seek(offset)
            target_fh.write(self.fh.read(length))
            #shutil.copyfileobj(self.fh, target_fh, length)
            # this method appears to be much, much slower

        return path


    def extractAll(self, path='.'):
        """Extract all media into the specified location."""

        for key in self.index:
            self.extract(key, path)


    def __verify(self):
        """Verify the file header to make sure this is indeed an RPA archive."""

        self.fh.seek(0)

        if self.__class__.HEADER != self.fh.read(len(self.__class__.HEADER)) or self.fh.read(1) != ' ':
            raise Exception('Header is not valid.')

        self.offset = long(self.fh.read(self.__class__.OFFSET_LENGTH), 16)

        key_str = '%x' % self.__class__.KEY
        if self.fh.read(2 + len(key_str)) != ' ' + key_str + '\n':
            raise Exception('Key is not valid.')


    def __loadIndex(self):
        """Read the file index containing all list of files and related metadata."""

        self.fh.seek(self.offset)
        raw_data = self.fh.read()
        uncompressed_data = raw_data.decode('zlib')
        self.index = loads(uncompressed_data)


if __name__ == "__main__":
    try:
        archive = RPA(sys.argv[1])
    except IndexError:
        print 'Usage:\n\tpython %s path/to/data.rpa [path/to/extract/to]' % __file__
        sys.exit(0)
    except Exception, e:
        print 'Error:', e
        sys.exit(1)

    try:
        archive.extractAll(sys.argv[2])
    except IndexError:
        archive.extractAll()
